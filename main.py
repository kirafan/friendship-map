import json

category_names = {
    1: {'name': 'story', 'path': 'Story'},
    2: {'name': 'event', 'path': 'Event'},
    3: {'name': 'chara', 'path': 'CharaEvent'},
    4: {'name': 'chara', 'path': 'CrossEvent'},
    5: {'name': 'weapon', 'path': 'WeaponEvent'},
    6: {'name': 'other', 'path': 'Other'},
    7: {'name': 'writer', 'path': 'Writer'},
}

category_weights = {
    1: 1,
    2: 1,
    3: 1,
    4: 5,
    5: 1,
    6: 1,
    7: 1,
}

with open('../database/advdatabase/ADVCharacterList.json') as f:
    character_list = json.load(f)
    characters = {}
    keys = set()
    for character in character_list:
        characters[character['m_ADVCharaID']] = character
        # if character['m_NamedType'] == -1:
        #     character['m_NamedType'] = character['m_CueSheet']
        keys.add(character['m_NamedType'])
    keys = sorted(keys)


friendship_map = {}
for a in keys:
    friendship_map[a] = {}
    for b in keys:
        if a <= b:
            friendship_map[a][b] = {
                'friendship': 0,
                'scenarios': [],
            }
        else:
            friendship_map[a][b] = friendship_map[b][a]


with open('../database/database/ADVList.json') as f:
    adv_list = json.load(f)

handled = set()
for adv in adv_list:
    if not adv['m_ScriptTextName']:
        continue
    name = '../database/advscript/%s/ADVScriptText_%s_%s.json' % (
        category_names[adv['m_Category']]['name'],
        category_names[adv['m_Category']]['path'],
        adv['m_ScriptTextName']
    )
    if name in handled:
        continue

    scenario = {}
    with open(name) as f:
        text = json.load(f)
    for line in text:
        name = line['m_charaName'].split('$')[0]
        named_type = characters.get(name, {'m_NamedType': -1})['m_NamedType']
        scenario[named_type] = scenario.get(named_type, 0) + 1
    for a in scenario:
        for b in scenario:
            friendship_map[a][b]['friendship'] += scenario[a] * \
                category_weights[adv['m_Category']]
            friendship_map[a][b]['scenarios'].append(adv['m_AdvID'])
    handled.add(name)

with open('../database/database/NamedList.json') as f:
    named_list = json.load(f)
    names = {
        named['m_NamedType']: named
        for named in named_list
    }

with open('../database/database/TitleList.json') as f:
    title_list = json.load(f)

for title in title_list:
    title_type = title['m_TitleType']
    if title_type == 22:
        continue
    with open('%s.csv' % title_type, 'w') as f:
        if True:
            f.write('id')
            f.write(',')
            for a in keys:
                f.write(names.get(a, {'m_FullName': 'その他'})['m_FullName'])
                f.write(',')
            f.write('\n')
        for named in named_list:
            if named['m_TitleType'] != title_type:
                continue
            named_type = named['m_NamedType']
            f.write(names.get(named_type, {'m_FullName': 'その他'})['m_FullName'])
            f.write(',')
            for a in keys:
                f.write(str(friendship_map[a][named_type]['friendship']))
                f.write(',')
            f.write('\n')


# if True:
#     print('id', end=',')
#     for a in keys:
#         print(a, end=',')
#     print()
#     for a in keys:
#         print(a, end=',')
#         for b in keys:
#             print(friendship_map[a][b]['friendship'], end=',')
#         print()
